<!-- Please fill out the template below to orphan your package. -->
<!-- This issue is processed by a script, so please do not modify the section headers or their order -->

## Request

``` yaml
# Replace foo with the package you wish to retire.
package_name: foo
# Please delete all of the items except the branch(es) that you're orphaning.
branches:
    - epel7
    - epel8
    - epel8-next
    - epel9
    - epel9-next
# Set this to false if your package still has (an)other active EPEL branch(es)
# that you do not wish to orphan.
all_branches: true
```

## Extra Information

<!-- Include any extra information or rationale here -->
